<?php
/**
 *	Template Name: Homepage
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package empower-lac
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

	</div>
	</div> <!-- Closing the container temporarily for the banner -->

    <?php
        include 'template-parts/carousel.php';
    ?>

	<div class="container">

		<h2 class="latest-posts-title">Latest Posts</h2>

		<div class="latest-posts">

			<ul class="recent-posts">
				<?php
				$args = array( 'numberposts' => '3', 'tax_query' => array(
					array(
						'taxonomy' => 'post_format',
						'field' => 'slug',
						'terms' => 'the-team',
						'operator' => 'NOT IN'
					),
					array(
						'taxonomy' => 'post_format',
						'field' => 'slug',
						'terms' => 'the-team',
						'operator' => 'NOT IN'
					)
				) );
				$recent_posts = wp_get_recent_posts( $args );
				foreach( $recent_posts as $recent ){
					echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   ( __($recent["post_title"])).'</a> </li> ';
				}
				wp_reset_query();
				?>

				<li>
					<a href="blog">More posts ></a>
				</li>
			</ul>

		</div>

	</div>

	<div class="homepage-sections">

		<section class="about-section">
			<div class="container-fluid">
				<div class="row no gutters">
					<div class="col-12 col-md-6 light-pod">
						<h2>About Us</h2>
						<p><b>Project: Empower</b> is a Social Enterprise project based in North Kensington. Our mission is to positively influence people’s lives and to promote a rights-based approach to access to justice.</p>
						<a class="cta" href="about">Read more ></a>
					</div>
					<div class="col-12 col-md-6 dark-pod">
						<h2>Meet the Team</h2>
						<p>Everyone at Project: Empower plays a vital role in the running of this project.</p>
						<a class="cta" href="the-team">Meet Us ></a>
						<br>
					</div>
				</div>

			</div>
		</section>

		<section class="quote-section padded testimonial">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-8 col-lg-10 offset-md-2 offset-lg-1">
						<h2>I didn't expect to see anyone as good as Mozes. He is approachable, sympathetic & has good understandings of victim of injustice like me.</h2>
					</div>
				</div>
			</div>
		</section>

		<section class="service-section padded">
			<div class="bg">
				<div class="container">
					<div class="row">
						<div class="col-12 col-md-6 text-holder">
							<div>
								<h2>Employment Drop-In Sessions</h2>
								<h4>No appointments necessary.</h4>
								<br>
								<h4>Every Wednesday from 12:00 - 14:00.</h4>

								<hr>

								<h5>Next Drop-In Date will be on <span style="color: #ddb800"><?php print date('j F', strtotime('next wednesday')); ?></span>.</h5>
							</div>
						</div>

						<div class="col-12 col-md-6">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.047250855653!2d-0.2180075838435496!3d51.51234911816162!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760fdf1bf8dd1b%3A0x8bcb525b35799e5f!2sLondon+W11+4AT!5e0!3m2!1sen!2suk!4v1554329032736!5m2!1sen!2suk" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="quote-section padded">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-8 offset-md-2">
						<h2>Working for Justice & Fairness</h2>
					</div>
				</div>
			</div>
		</section>

		<section class="about-section">
			<div class="container-fluid">
				<div class="row no gutters">
					<div class="col-12 col-md-6 dark-pod">
						<h2>Donate</h2>
						<p>Your donation could help keep our <b>FREE</b> service running, providing vital legal help to disadvantaged families who need it most.</p>
						<a class="cta" href="donate">Donate</a>
					</div>
					<div class="col-12 col-md-6 light-pod">
						<h2>Join Us</h2>
						<p>We're always on the lookout for bright-minded individuals to join our diverse & hardworking team.</p>
						<p>Think you've got what it takes to join us?</p>
						<a class="cta" href="about">Join Us ></a>
					</div>
				</div>

			</div>
		</section>

		<section class="quote-section padded">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-8 offset-md-2">
						<h2>We value diversity, promote equality and challenge discrimination.</h2>
					</div>
				</div>
			</div>
		</section>

	</div>

	</main><!-- #main -->
	</div><!-- #primary -->


<?php

get_footer();
