<div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active slide-1">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1>Access to legal advice has never been easier.</h1>
                    <p class="lead">We promote a rights-based approach to access to justice.</p>
                    <br>
                    <a href="./about" class="cta">Find out more</a>
                </div>
            </div>
        </div>
        <div class="carousel-item slide-2">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1>Harnessing community power</h1>
                    <p class="lead">We educate and inform people about their rights in practical and powerful way to increase public understanding about the law.</p>
                    <br>
                    <a href="./about" class="cta">Find out more</a>
                </div>
            </div>
        </div>
        <div class="carousel-item slide-3">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1>Empowering citizens</h1>
                    <p class="lead">We educate and inform people about their rights and responsibilities which creates empowered citizens who understand and value the rule of law. </p>
                    <br>
                    <a href="./about" class="cta">Find out more</a>
                </div>
            </div>
        </div>
        <div class="carousel-item slide-4">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1>Social Mobility</h1>
                    <p class="lead">We aim to aid the attraction of more diverse talent to the legal industry.</p>
                    <br>
                    <a href="./about" class="cta">Find out more</a>
                </div>
            </div>
        </div>
    </div>

    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>