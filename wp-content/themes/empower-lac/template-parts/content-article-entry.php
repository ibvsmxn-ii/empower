<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package empower-lac
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
		<?php

		if ( 'post' === get_post_type() ) :
			?>
		<?php endif; ?>
    </header><!-- .entry-header -->

    <div class="entry-content col-12 col-md-10 col-lg-8">
		<?php
		the_content( sprintf(
			wp_kses(
			/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'empower-lac' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'empower-lac' ),
			'after'  => '</div>',
		) );
		?>
    </div><!-- .entry-content -->

    <div class="entry-meta">
	    <?php echo get_avatar( $id_or_email, $size, $default, $alt, $args ); ?>
        &nbsp;
		<?php
		empower_lac_posted_on();
		empower_lac_posted_by();
		?>
    </div><!-- .entry-meta -->

</article><!-- #post-<?php the_ID(); ?> -->
