<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package empower-lac
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>');">
    <div class="pod-content">
        <header class="entry-header">
		    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->

        <hr>

        <div class="entry-content">
		    <?php
		    the_content();

		    wp_link_pages( array(
			    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'empower-lac' ),
			    'after'  => '</div>',
		    ) );
		    ?>
        </div><!-- .entry-content -->

	    <?php if ( get_edit_post_link() ) : ?>
            <footer class="entry-footer">
			    <?php
			    edit_post_link(
				    sprintf(
					    wp_kses(
					    /* translators: %s: Name of current post. Only visible to screen readers */
						    __( 'Edit <span class="screen-reader-text">%s</span>', 'empower-lac' ),
						    array(
							    'span' => array(
								    'class' => array(),
							    ),
						    )
					    ),
					    get_the_title()
				    ),
				    '<span class="edit-link">',
				    '</span>'
			    );
			    ?>
            </footer><!-- .entry-footer -->
	    <?php endif; ?>
    </div>

</article><!-- #post-<?php the_ID(); ?> -->
