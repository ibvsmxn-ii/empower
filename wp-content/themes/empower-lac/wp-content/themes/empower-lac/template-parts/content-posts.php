<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package empower-lac
 */

?>

<div class="col-12 col-md-6 col-lg-4 blog-listing-post">

	<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="background-image: url('<?php echo $backgroundImg[0]; ?>'); background-repeat: no-repeat; background-size: cover;">
        <header class="entry-header">
			<?php
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			endif;

			if ( 'post' === get_post_type() ) :
				?>
                <div class="entry-meta">
					<?php
					empower_lac_posted_on();
					empower_lac_posted_by();
					?>
                </div><!-- .entry-meta -->
			<?php endif; ?>
        </header><!-- .entry-header -->

    </article><!-- #post-<?php the_ID(); ?> -->
</div>

