<?php
/**
 *	Template Name: Team Listings Page
 * Template Post Type: post, page, product
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package empower-lac
 */

get_header();
?>

    </div>

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4"><?php wp_title() ?></h1>
        </div>
    </div>

    <div class="container">

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>


			<?php $catquery = new WP_Query( 'cat=4' ); ?>
            <ul class="team-listing row no-gutters">
				<?php while($catquery->have_posts()) : $catquery->the_post(); ?>
                    <li class="person col-12 col-sm-6 col-md-4 col-lg-3" style="background-image: url('<?php the_post_thumbnail_url( $size ); ?> ');">
                        <h3>
                            <a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a>
                            <span class="job-title"><?php the_field('job_title'); ?></span>
                        </h3>
                    </li>
				<?php endwhile; ?>
            </ul>
			<?php wp_reset_postdata(); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
