<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package empower-lac
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-12">

                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    Project: Empower
                    <br>
                    <a class="link" href="https://www.google.com/maps/place/North+Kensington+Law+Centre/@51.5123943,-0.2181037,17z/data=!3m1!4b1!4m5!3m4!1s0x417031500ada1061:0x4e4af177ac11b56!8m2!3d51.512391!4d-0.215915" target="_blank">Unit 13 Baseline Studios, Whitchurch Road, London, W11 4AT</a>
                    <br>

                    <hr>

                    <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'empower-lac' ) ); ?>">
                            &copy; <?php
	                        $copyYear = 2018; // Set your website start date
	                        $curYear = date('Y'); // Keeps the second year updated
	                        echo $copyYear . (($copyYear != $curYear) ? '-' . $curYear : '');
	                        ?> Empower LAC.
                    <br>
                        Website produced by D'André Phillips
                    <br>
                        <br>
                        <br>
                        <a href="https://www.nklc.co.uk/"><img src="../wp-content/uploads/2019/03/north-kensington-law-centre.png"></a>
                        <a href="https://www.westminster.ac.uk/"><img src="../wp-content/uploads/2019/03/university-of-westminster.jpg"></a>
                </div>
                <div class="col-12 col-md-6">
                    <div class="row">
                        <div class="social">
                            <a href="https://www.linkedin.com/company/empowerlac/about/" target="_blank"><i class="fab fa-linkedin"></i></a>
                            <a href="https://www.facebook.com/EmpowerLAC/" target="_blank"><i class="fab fa-facebook"></i></a>
                            <a href="https://twitter.com/empowerlac" target="_blank"><i class="fab fa-twitter"></i></a>
                            <a href="https://www.instagram.com/empowerlac/" target="_blank"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                    <div class="row">
                        <ul class="legal-links">
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <div class="row">
                        <span class="tagline">Challenge. Educate. Empower.</span>
                    </div>
                </div>


        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
