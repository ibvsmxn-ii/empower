<?php
/**
 *	Template Name: Team Member Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package empower-lac
 */

get_header();
?>

    </div>

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4"><?php wp_title() ?></h1>
	        <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

            <div class="header-wrap" style="background: url('<?php echo $backgroundImg[0]; ?>') no-repeat;">
                <header class="entry-header">
                    <h1 class="entry-title"><?php the_title(); ?></h1>
                </header>
            </div>
        </div>
    </div>

    <div class="container">

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

            <H1>IT WORKS!</H1>

			<?php $catquery = new WP_Query( 'cat=4' ); ?>
            <ul class="team-listing">
				<?php while($catquery->have_posts()) : $catquery->the_post(); ?>
                    <li>
                        <h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                        <span class="job-title">Job Title</span>
                    </li>
				<?php endwhile; ?>
            </ul>
			<?php wp_reset_postdata(); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
