<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package empower-lac
 */

get_header();
?>

</div>

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4"><?php wp_title() ?></h1>
        </div>
    </div>

    <div class="container">

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
            <div class="col-12 col-md-10 col-xl-8">
	            <?php
	            while ( have_posts() ) :
		            the_post();

		            get_template_part( 'template-parts/content', 'page' );

		            // If comments are open or we have at least one comment, load up the comment template.
		            if ( comments_open() || get_comments_number() ) :
			            comments_template();
		            endif;

	            endwhile; // End of the loop.
	            ?>
            </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
