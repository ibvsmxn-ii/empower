<?php
/**
 * Template Name: Team Member Page
 * Template Post Type: post, page, product
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package empower-lac
 */

get_header();
?>

    </div>

<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

    <div class="jumbotron jumbotron-fluid" style="background: url('<?php echo $backgroundImg[0]; ?>') no-repeat; background-position: right; background-size: contain; background-attachment: fixed;">
        <div class="container">
            <h1 class="display-4"><?php wp_title() ?></h1>
            <div class="header-wrap">
                <header class="entry-header">
                    <h3 class="job-title"><?php the_field('job_title'); ?></h3>
                </header>
            </div>
        </div>
    </div>

    <div class="container">

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <div class="col-12 col-md-8">
	            <?php
	            while ( have_posts() ) :
		            the_post();

		            get_template_part( 'template-parts/content-employee', get_post_type() );

	            endwhile; // End of the loop.
	            ?>
            </div>

            <div class="col-12 col-md-4">

            </div>


        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
